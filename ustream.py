#!/usr/bin/python3

import re
import logging
import uamf0
import urllib.request

logging.basicConfig(level=logging.DEBUG)

USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725'
RE_EMBED = re.compile('/embed/([0-9]+)')
SWF_URL = 'http://static-cdn1.ustream.tv/swf/live/viewer.rsl:383.swf'

class UstreamException(Exception):
    pass

def get_id_from_embed_url(url):
    m = RE_EMBED.search(url)
    if m is None:
        raise UstreamException("Failed to extract id from stream url")
    return int(m.group(1))

def get_raw_info(stream_id):
    url = "http://cdngw.ustream.tv/Viewer/getStream/1/{0}.amf".format(stream_id)
    r = urllib.request.Request(url)
    r.add_header('User-Agent', USER_AGENT)

    with urllib.request.urlopen(r) as f:
        buf = f.read()
    return buf

if __name__ == '__main__':
    EXAMPLE_EMBED = 'http://www.ustream.tv/embed/12727303?autoplay=true'
    stream_id = get_id_from_embed_url(EXAMPLE_EMBED)
    logging.debug("stream id is {0}".format(stream_id))

    raw_info = get_raw_info(stream_id)
    logging.debug("got {0} bytes from ustream".format(len(raw_info)))
    info_parsed = uamf0.parse_netconnection_call(raw_info)
    print(info_parsed)
    import pprint
    pprint.pprint(info_parsed.messages[0].value)
