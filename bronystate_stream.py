#!/usr/bin/python3

import os
import re
import urllib.request
import socket
import logging

# local
import ustream
import uamf0

URL_EMBED = 'http://www.bronystate.net/wp-content/plugins/sk_bs_theater/embed.shtml'
USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725'
RE_IFRAME = re.compile('<iframe[^>]+src="([^"]+)"')

logging.basicConfig(level=logging.DEBUG)

class Ustream:
    def __init__(self, stream_id):
        self.stream_id = stream_id

    def get_rtmp_parameters(self):
        raw_info = ustream.get_raw_info(self.stream_id)
        info_call = uamf0.parse_netconnection_call(raw_info)
        msg = info_call.messages[0].value
        if msg['status'] != 'online':
            raise Exception("stream status is {0}".format(msg['status']))
        if 'fmsUrl' in msg:
            fmsUrl = msg['fmsUrl'] + "/"
            return { 'rtmp': fmsUrl,
                     'playpath': 'streams/live',
                     'swfVfy': ustream.SWF_URL }
        elif 'streamVersions' in msg:
            streamVersions = list(msg['streamVersions'].items())
            logging.info("found {0} stream versions".format(len(streamVersions)))
            logging.debug("stream versions: {0!r}".format(streamVersions))
            logging.info("cdn url is {0}".format(msg['cdnUrl']))
            name, info = streamVersions[0]
            logging.info("choosing stream version {0}".format(name))
            return { 'rtmp': msg['cdnUrl'],
                     'playpath': info['streamVersionCdn']['level3']['cdnStreamName'],
                     'swfVfy': ustream.SWF_URL }
        elif 'streamName' in msg:
            return { 'rtmp': msg['cdnUrl'],
                     'playpath': msg['streamName'],
                     'swfVfy': ustream.SWF_URL }
        else:
            raise Exception("could not determine rtmp parameters")
        
    @classmethod
    def from_url(cls, url):
        stream_id = ustream.get_id_from_embed_url(url)
        return cls(stream_id)

class Limev:
    re_name = re.compile("[&?]fileid=([^&]+)")
    dns_address = "live.limev.com"
    
    def __init__(self, name):
        self.name = name
        self.ip = None
        # XXX: limev's load balancer is not supported yet
        self.ip = "95.211.196.30"

    def get_rtmp_parameters(self):
        if self.ip is None:
            self.ip = self.fetch_ipv4_address()
            
        app = 'live/_definst_'
        return { 'host': self.ip,
                 'app': app,
                 'swfVfy': "http://cdn.yycast.com/player/player.swf",
                 'flashVer': 'LNX 11,2,202,251',
                 'tcUrl': 'rtmp://{0}/{1}'.format(self.ip, app),
                 'pageUrl': "http://www.limev.com/embed.php?fileid={0}&vw=640&vh=480".format(self.name),
                 'playpath': self.name }
        
    @classmethod
    def fetch_ipv4_address(cls):
        addr = socket.gethostbyname(cls.dns_address)
        return addr
        
    @classmethod
    def from_url(cls, url):
        m = cls.re_name.search(url)
        if m is None:
            raise Exception("could not extract stream name from url {!r}".format(url))
        name = m.group(1)
        return cls(name)
        
    def __repr__(self):
        return "Limev({!r})".format(self.name)

class Veemi:
    re_name = re.compile("[&?]v=([^&]+)")
    dns_address = "live.veemi.com"
    
    def __init__(self, name):
        self.name = name
        #self.ip = None
        # XXX: need to override ip - live.veemi.com uses weird/unsupported rtmp redirect
        self.ip = '96.44.149.50'

    def get_rtmp_parameters(self):
        if self.ip is None:
            self.ip = self.fetch_ipv4_address()
            
        app = 'live/_definst_'
        return { 'host': self.ip,
                 'app': app,
                 'flashVer': 'LNX 11,2,202,251',
                 'swfVfy': 'http://www.veemi.com/player/player-licensed.swf',
                 'tcUrl': 'rtmp://{0}/{1}'.format(self.ip, app),
                 'pageUrl': "http://www.veemi.com/embed.php?v={0}&vw=640&vh=480".format(self.name),
                 'playpath': self.name }
        
    @classmethod
    def fetch_ipv4_address(cls):
        addr = socket.gethostbyname(cls.dns_address)
        return addr
        
    @classmethod
    def from_url(cls, url):
        m = cls.re_name.search(url)
        if m is None:
            raise Exception("could not extract stream name from url {!r}".format(url))
        name = m.group(1)
        return cls(name)
        
    def __repr__(self):
        return "Veemi({!r})".format(self.name)

def grab_embed_iframe():
    r = urllib.request.Request(URL_EMBED)
    r.add_header('User-Agent', USER_AGENT)
    f = urllib.request.urlopen(r)
    buf = f.read().decode('utf-8')
    
    m = RE_IFRAME.search(buf)
    if m is None:
        raise Exception("failed to locate iframe in page source")
    
    return m.group(1)

class Twitch:
    def __init__(self, channel_name):
        self.channel_name = channel_name

    def get_rtmp_parameters(self):
        import twitch
        channel_xml = twitch.grab_channel_xml(self.channel_name)
        streams = twitch.extract_streams(channel_xml)
        if not streams:
            raise Exception("no twitch streams found")
        return twitch.get_rtmpdump_parameters(streams[0])

    @classmethod
    def from_url(cls, url):
        channel_name = re.search("channel=([^$&]*)", url)
        return cls(channel_name.group(1))

def parse_stream_url(url):
    if url.startswith("http://www.veemi.com/embed.php"):
        return Veemi.from_url(url)
    elif url.startswith("http://www.limev.com/embed.php"):
        return Limev.from_url(url)
    elif url.startswith("http://www.ustream.tv/embed"):
        return Ustream.from_url(url)
    elif url.startswith("http://www.twitch.tv/widgets/live_embed_player.swf"):
        return Twitch.from_url(url)
    else:
        raise Exception("Don't know how to handle url {!r}".format(url))

if __name__ == "__main__":
    url = grab_embed_iframe()
    logging.debug("Embed URL is {0}".format(url))

    handler = parse_stream_url(url)

    params = handler.get_rtmp_parameters()
    logging.debug("RTMP parameters are {0!r}".format(params))

    args = [x for y in [("--" + a, b) for (a,b) in params.items()] for x in y]

    os.execvp("rtmpdump", ["rtmpdump", "--live", "--quiet"] + args)
