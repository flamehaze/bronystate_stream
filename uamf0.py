#!/usr/bin/python3

import struct
from io import BytesIO

class StreamEOF(Exception):
    pass

class DecodeException(Exception):
    pass

def read_bytes(stream, n):
    data = stream.read(n)
    if len(data) != n:
        raise StreamEOF()
    return data

def read_uint16(stream):
    buf = read_bytes(stream, 2)
    return (buf[0] << 8) | buf[1]

def read_uint32(stream):
    buf = read_bytes(stream, 4)
    return struct.unpack("!I", buf)[0]

class Decoder:
    def __init__(self, data=None, decode_strings=True):
        if data is None:
            data = b''
        self.stream = BytesIO(data)
        self.decode_strings = decode_strings
            
    def read_string(self):
        length = read_uint16(self.stream)
        data = read_bytes(self.stream, length)
        if self.decode_strings:
            return data.decode('UTF-8')
        else:
            return data

    def read_number(self):
        data = read_bytes(self.stream, 8)
        return struct.unpack("!d", data)[0]

    def read_array(self):
        num_elem = read_uint32(self.stream)
        val = []
        for i in range(num_elem):
            val.append(self.read_typed_value())
        return val

    def read_boolean(self):
        data = read_bytes(self.stream, 1)
        return data[0] != 0

    def read_null(self):
        return None

    def read_typed_value(self):
        type_byte = read_bytes(self.stream, 1)[0]
        if type_byte == 0x00:
            value = self.read_number()
        elif type_byte == 0x01:
            value = self.read_boolean()
        elif type_byte == 0x02:
            value = self.read_string()
        elif type_byte == 0x03:
            value = self.read_object()
        elif type_byte == 0x05:
            value = self.read_null()
        elif type_byte == 0x0a:
            value = self.read_array()
        else:
            raise DecodeException("unknown type 0x{0:x}".format(type_byte))
        return value

    def read_object(self):
        pairs = []
        while True:
            key = self.read_string()
            if key:
                value = self.read_typed_value()
                pairs.append( (key, value) )
            else:
                endmarker = read_bytes(self.stream, 1)[0]
                if endmarker != 0x09:
                    raise DecodeException("Expected end-of-object marker (0x09) but got 0x{0:x} instead".format(endmarker))
                break
        return dict(pairs)

class NCCall:
    __slots__ = ['headers', 'messages']
    def __repr__(self):
        return ("<NCCall"
                " headers={0.headers}"
                " messages={0.messages}".format(self))

class NCHeader:
    __slots__ = ['name', 'must_understand', 'value']
    def __repr__(self):
        return ("<NCHeader"
                " name={0.name}"
                " must_understand={0.must_understand}"
                " value={0.value}>".format(self))

class NCMessage:
    __slots__ = ['target', 'response', 'value']
    def __repr__(self):
        return ("<NCMessage"
                " target={0.target}"
                " response={0.response}"
                " value={0.value}>".format(self))

def _parse_netconnection_call(d):
    amf_type = read_bytes(d.stream, 2)
    assert amf_type == b'\x00\x00' # amf0

    # == headers ==
    headers = []
    header_count = read_uint16(d.stream)
    for i in range(header_count):
        header = NCHeader()
        header.name = d.read_string()
        header.must_understand = d.read_boolean()

        # TODO: should we check the length?
        msg_len = read_uint32(d.stream)
        
        header.value = d.read_typed_value()
        headers.append(header)

    # == messages ==
    messages = []
    msg_count = read_uint16(d.stream)
    for i in range(msg_count):
        m = NCMessage()
        m.target = d.read_string()
        m.response = d.read_string()

        # TODO: should we check the length?
        msg_len = read_uint32(d.stream)
        
        m.value = d.read_typed_value()
        messages.append(m)

    m = NCCall()
    m.headers = headers
    m.messages = messages
    return m

def parse_netconnection_call(o):
    if not isinstance(o, Decoder):
        o = Decoder(o)
    return _parse_netconnection_call(o)

if __name__ == '__main__':
    with open('tmp.out', 'rb') as f:
        buf = f.read()
        d = Decoder(buf)
        print(parse_netconnection_call(d))
        
