#!/usr/bin/python3

import re
import logging
import urllib.request
import xml.etree.ElementTree as etree
from pipes import quote

USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725'
logging.basicConfig(level=logging.DEBUG)

def grab_channel_xml(channel_name):
    url = "http://usher.twitch.tv/find/{0}.xml?type=live".format(channel_name)
    logging.debug("Fetching url {0}".format(url))
    r = urllib.request.Request(url)
    r.add_header('User-Agent', USER_AGENT)
    with urllib.request.urlopen(r) as f:
        buf = f.read()
    return buf.decode('UTF-8')

def extract_streams(channel_xml):
    logging.debug("Extracting streams")
    xml = etree.fromstring(channel_xml)
    streams = []
    for livestream in xml.findall("live"):
        data = { 'playpath': livestream.find("play").text,
                 'token': livestream.find("token").text,
                 'rtmp': livestream.find("connect").text }
        streams.append(data)
    logging.debug("found {0} streams".format(len(streams)))
    return streams

def get_playerpath():
    url = "http://www-cdn.jtvnw.net/swflibs/TwitchPlayer.swf"
    logging.debug("Fetching url: {0}".format(url))
    r = urllib.request.Request(url)
    r.add_header('User-Agent', USER_AGENT)
    with urllib.request.urlopen(r) as f:
        resolved_url = f.geturl()
        logging.debug("Player url: {0}".format(resolved_url))
        return f.geturl()

def get_rtmpdump_parameters(stream):
    return { 'rtmp': stream['rtmp'],
             'playpath': stream['playpath'],
             'jtv': stream['token'],
             'swfVfy': get_playerpath() }

if __name__ == '__main__':
    import shlex
    import sys
    import os
    if len(sys.argv) < 2:
        sys.stderr.write("Usage: {0} <channel_name>".format(sys.argv[0]))
        sys.exit(1)
    channel_name = sys.argv[1]
    channel_xml = grab_channel_xml(channel_name)
    streams = extract_streams(channel_xml)
    if not streams:
        sys.stderr.write("no streams found\n")
        sys.exit(1)
    params = get_rtmpdump_parameters(streams[0])
    args = [x for y in [("--" + a, b) for (a,b) in params.items()] for x in y]
    args = ["--live", "--quiet"] + args
    logging.debug("executing rtmpdump " + " ".join(map(quote, args)))
    os.execvp("rtmpdump", args)
